#!/usr/bin/env python3
from setuptools import setup, find_packages
import os 
from pathlib import Path

readme_file = Path(__file__).parent / 'README.md'
version_file =  Path(__file__).parent / 'explora_hat/_version.py' 
exec(open(version_file).read()) # sets version

# load the README file and use it as the long_description for PyPI
with open(readme_file, 'r') as f:
    readme = f.read()

# package configuration - for reference see:
# https://setuptools.readthedocs.io/en/latest/setuptools.html#id9
setup(
    name='explora_hat',
    description="Explora's Raspberry pi HAT controller",
    long_description=readme,
    long_description_content_type='text/markdown',
    version=version,
    author='Bruno Laurencich',
    author_email='bruno@chordata.cc',
    url='https://www.mdbr.it/',
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.8",
    install_requires=['gpiozero', 'RPi.GPIO', 'SimpleWebSocketServer', "fastapi[all]" ],
    license="GNU GPLv3",
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='hardware gpio ws2812 ws281x led button raspberry '
)
