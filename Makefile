all: build develop


explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x/python/setup.py: install-deps
	@echo Building C WS281x Library
	cd explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x && scons
	cd explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x/python && python3 setup.py build
	cp explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x/python/build/lib.linux-a*/* explora_hat/ws281x


build: explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x/python/setup.py

explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x:
	rm -rf explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x
	sudo apt install -y scons swig python3-dev git
	-git clone https://github.com/jgarff/rpi_ws281x explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x
	cd explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x && git -c advice.detachedHead=false checkout 5d0041e

install-deps: explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x

venv:
	@echo Creating venv
	python -m venv venv

.explora_hat_installed:venv
	@echo Installing explora_hat module in venv
	venv/bin/pip install -e .
	touch .explora_hat_installed
	@echo explora_hat module sucessfully installed in venv

develop: .explora_hat_installed

clean:
	rm -rf explora_hat/ws281x/lib/rpi_ws281x/rpi_ws281x venv .explora_hat_installed