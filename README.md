# Explora's Raspberry pi HAT controller

Python controller for the Explora's Raspberry pi HAT. The HAT wxposes GPIO pins trough screw terminals, making it easy to deploy interactive installations like the ones found on [Rome's Explora museum](https://www.mdbr.it/) 

It features:

- 6x button inputs
- 6x high voltage / high current output driver based on the [ULN2003 darlignton array](https://www.ti.com/lit/ds/symlink/uln2003a.pdf)
- 1x PWM output. It can be configured to drive a [ws281x RGB led strip](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) or set as a regular GPIO input/output
- A perfboard area to add extra components 
- Selectable power source input for the ULN2003 driver:
	- Raspberry's 3v3 rail (not recomended, only for testing purpouses)
	- Raspberry's 5V rail
	- Screw terminal input
	- Barrel Jack input

![explora HAT mounted on raspberry pi 3](explora_HAT_mounted_rpi.jpg)

## Requeriments

Requires python >=3.8
Compilation is required if the ws281x module is used. It is performed automatically at `setup.py`.
The `ws281x` module requires access to `/dev/mem`. The program needs to be run as root when this module is used/

## Installation

The following snippet will install the module in editable mode, changes can be made to the code and it will be reflected on the imported module

```bash
git clone https://gitlab.com/explora_mdbr/rpi-explora-hat.git
cd rpi-explora-hat
make
venv/bin/pip install -e .
```

## Usage examples

More at the `examples` folder

### Dev display server

The display server is a simple webapp that receives commands thought websocket and displays different fullscreen plates coming from images or videos.
Will start the display server at port 8080 create a concurrent task that will change the
frontend states between: ST1 and ST2.

```python
from explora_hat import display_server
import asyncio


# Create an event and set it when the first Ws client is connected
ready = asyncio.Event()
display_server.ws_manager.connected_callback = lambda w: ready.set()

states = ["ST1", "ST2"]

async def change_states():
	"""This task will change periodically the state of the frontend"""
	try:
		await ready.wait() # wait here for the first client to be connected
		print("Init Websocket task")
		current_state = 0
		while 1:
			# Send to all connected clients the new state
			await display_server.ws_manager.broadcast("sent from cli WS server", states[current_state])
			current_state = (current_state + 1) % len(states) 
			await asyncio.sleep(2)
   
	except Exception as e:
		print("WS task error:", e)

# Get the current event loop
loop = asyncio.new_event_loop()
# Add a task to the event loop
task = loop.create_task(change_states())
# Set a handler on server to cancel the task in case it's still pending
display_server.on_end = lambda app: task.cancel() 

try:
	# Run the display server, passing the current event loop
	# This allows to use custom defined tasks
    display_server.main(loop=loop)

except asyncio.CancelledError:
	pass

```

Some parameters of the display server can be configured like:

``` python
# set static directory from where to serve HTTP resources, including index.html, etc:
display_server.STATIC_DIR = "/path/to/public/dir"
# set host, for example to allow incoming connections from other devices in the network:
display_server.PORT = "0.0.0.0"
# set HTTP and WS ports:
display_server.PORT = 8081
```

### Ws281x RGB button controller

Will light a `ws281x` led strip using 3 buttons for the RGB components and an extra button for a white light.

```python
from explora_hat import ws281x as led_strip
from explora_hat import gpio_manager
from time import sleep

for i in range(2):
	led_strip.set_leds([led_strip.Color(255,0,255),led_strip.Color(255, 255),led_strip.Color(g = 255, b =255)])
	sleep(0.5)
	led_strip.set_leds([0,0,0])
	sleep(0.2)

OFF = 0
ON = 255
NUM_LEDS = 30

rgb_state = [OFF, OFF, OFF]
w_button = False

the_color = led_strip.Color(OFF, OFF, OFF)
btns = gpio_manager.Button_Manager()

def update_strip():
	global w_button
	if w_button:
		the_color = led_strip.Color(ON,ON,ON)
	else:	
		the_color = led_strip.Color(*rgb_state)

	led_strip.set_leds([the_color]*NUM_LEDS)

def rgb_on(num, unused):
	global rgb_state
	print("BTN {} ON".format(num), rgb_state)
	rgb_state[num] = ON
	update_strip()

def rgb_off(num, unused):
	global rgb_state
	print("BTN {} OFF".format(num), rgb_state)
	rgb_state[num] = OFF
	update_strip()

def all_on(num, unused):
	global w_button
	print("ALL ON", rgb_state)

	w_button = True
	update_strip()

def all_off(num, unused):
	global w_button
	print("ALL OFF", rgb_state)

	w_button = False
	update_strip()

for i in range(3):
	btns.set_press_handler(i, rgb_on)
	btns.set_release_handler(i, rgb_off)

btns.set_press_handler(3, all_on)
btns.set_release_handler(3, all_off)


try:
	input("press a key to end")
	
finally:
	led_strip.close()
	
```
