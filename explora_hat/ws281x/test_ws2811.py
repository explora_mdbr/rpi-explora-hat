

from time import sleep as uwait

from struct import unpack
import socket
import time
import os

# import sys
# import os

# dir_path = os.path.dirname(os.path.realpath(__file__))
# lib_path = os.path.join(dir_path, "lib/rpi_ws281x/python/build/lib.linux-armv7l-3.5")

# ws2811 setup
from ws_2811_conf import ws, leds


ws.ws2811_fini(leds)
ws.delete_ws2811_t(leds)

print("running in py3 with no PYTHONPATH mod")