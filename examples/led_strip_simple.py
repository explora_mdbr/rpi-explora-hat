from explora_hat import ws281x as led_strip
from explora_hat import gpio_manager
from time import sleep

OFF = 0
MAX_V = 50
NUM_LEDS = 9

print("Lighting initial CMY sequence")
for i in range(3):
	led_strip.set_leds([led_strip.Color(MAX_V,0,MAX_V), #Magenta
						led_strip.Color(MAX_V, MAX_V),  #Yellow
						led_strip.Color(g = MAX_V, b =MAX_V) #Cyan
						] * int(NUM_LEDS / 3))
	sleep(0.2)
	led_strip.set_leds([OFF, OFF, OFF] * int(NUM_LEDS / 3))
	sleep(0.2)

color_a = led_strip.Color(245,45,45)  # red
color_b = led_strip.Color(245,242,44) # yellow
color_c = led_strip.Color(44,167,245) # blue

colors = [color_a, color_b, color_c]

for color in colors:
	color.v = MAX_V

print("Three color rotation")
try:
	input("press a ENTER to start, CTRL + C to end")
	for i,c in enumerate(colors):
		led_strip.set_leds([c]*NUM_LEDS)
		sleep(1)
		for factor in range(10):
			the_color = c.mix(colors[(i+4)%3], factor/10.0)
			led_strip.set_leds([the_color]*NUM_LEDS)
			sleep(2/10)
		sleep(1)

except KeyboardInterrupt:
	pass

the_color = led_strip.Color(OFF, OFF, OFF)
led_strip.set_leds([the_color]*NUM_LEDS)
led_strip.close()


	
