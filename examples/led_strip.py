from explora_hat import ws281x as led_strip
from explora_hat import gpio_manager
from time import sleep

for i in range(2):
	led_strip.set_leds([led_strip.Color(255,0,255),led_strip.Color(255, 255),led_strip.Color(g = 255, b =255)])
	sleep(0.5)
	led_strip.set_leds([0,0,0])
	sleep(0.2)

OFF = 0
ON = 55
NUM_LEDS = 10

rgb_state = [OFF, OFF, OFF]
w_button = False

the_color = led_strip.Color(OFF, OFF, OFF)
btns = gpio_manager.Button_Manager()

def update_strip():
	global w_button
	if w_button:
		the_color = led_strip.Color(ON,ON,ON)
	else:	
		the_color = led_strip.Color(*rgb_state)

	led_strip.set_leds([the_color]*NUM_LEDS)

def rgb_on(num, unused):
	global rgb_state
	print("BTN {} ON".format(num), rgb_state)
	rgb_state[num] = ON
	update_strip()

def rgb_off(num, unused):
	global rgb_state
	print("BTN {} OFF".format(num), rgb_state)
	rgb_state[num] = OFF
	update_strip()

def all_on(num, unused):
	global w_button
	print("ALL ON", rgb_state)

	w_button = True
	update_strip()

def all_off(num, unused):
	global w_button
	print("ALL OFF", rgb_state)

	w_button = False
	update_strip()

for i in range(3):
	btns.set_press_handler(i, rgb_on)
	btns.set_release_handler(i, rgb_off)

btns.set_press_handler(3, all_on)
btns.set_release_handler(3, all_off)


try:
	input("press a key to end")
	
finally:
	led_strip.close()
	
